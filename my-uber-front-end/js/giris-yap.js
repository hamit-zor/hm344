//Modal, girdiler ve hata mesajları tanımlanıyor.
var $modal = $(".modal");
var $modal_message = $(".modal-message");

var $close_btn = $(".btn-modal-close");
var $giris_btn = $("#btn-login");

var $email = $("#email");
var $password = $("#password");
var $valid_email = false;

var $err_email = "E-posta adresi boş bırakılamaz!<br>";
var $err_password = "Parola boş bırakılamaz!<br>";
var $err_valid_email = "Girilen E-posta adresi hatalıdır.<br>";

$modal_message.html('');
var $form_items = [$email, $password];
var $err_contents = [$err_email, $err_password];

$giris_btn.click(function () {
  $valid_email = true;
  //Girdilerin dolu olduğunu kontrol eden bölüm.
  $modal_message.html('');
  for (var $i = 0; $i < $form_items.length; $i++) {
    if ($form_items[$i].val() == '' && $modal_message.html().search($err_contents[$i]) == -1) {
      $form_items[$i].siblings(".input-req").removeClass("input-req-success");
      $form_items[$i].siblings(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_contents[$i]);
    } else if ($form_items[$i].val() != '' && $modal_message.html().search($err_contents[$i]) != -1) {
      $form_items[$i].siblings(".input-req").addClass("input-req-success");
      $form_items[$i].siblings(".input-req").removeClass("input-req-fail");
      $modal_message.html($modal_message.html().replace($err_contents[$i], ""));
    }
  }

  //E-posta adresinin geçerli olup olmadığını kontrol eden bölüm.
  if ($email.val() != '') {
    if (!isValidEmailAddress($email.val())) {
      $valid_email = false;
      $email.siblings(".input-req").removeClass("input-req-success");
      $email.siblings(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_valid_email);
    }
  }

  //Modal'ı gizleyen veya gösteren bölüm.(Titreme efekti ile.)
  if ($modal_message.html() != '') {
    $modal.css("display", "block");
    $modal.removeClass("vibrate");
    setTimeout('$modal.addClass("vibrate");', 100);
  }
  else {
    $modal.css("display", "none");
  }
});

//Modal'ı kapatan buton için click handler.
$close_btn.click(function () {
  $modal.css("display", "none");
});

$giris_btn.click(function () {
  $("#form").submit();
});

$("input").focus(function () {
  $(this).siblings(".input-req").removeClass("input-req-fail");
  $(this).siblings(".input-req").addClass("input-req-success");
});

//Girdilerden biri boşsa submit işlemini iptal eden fonksyon.
function login_valid() {
  console.log($valid_email);
  if ($email.val() == '' || $password.val() == '' || !$valid_email) {
    return false;
  }
}
//E-posta adresini kontrol eden fonksyon.
function isValidEmailAddress(emailAddress) {
  var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
  return pattern.test(emailAddress);
}