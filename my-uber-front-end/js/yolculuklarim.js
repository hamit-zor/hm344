//Modal, girdiler ve hata mesajları tanımlanıyor.
var $modal = $(".modal");
var $modal_message = $(".modal-message");

var $close_btn = $(".btn-modal-close");
var $tamamla_btn = $("#btn-filtrele");

var $baslangic = $("#baslangic");
var $bitis = $("#bitis");

var $err_baslangic = "Başlangıç tarihi boş bırakılamaz!<br>";
var $err_bitis = "Bitiş tarihi boş bırakılamaz!<br>";
var $err_baslangic_nodate = "Başlangıç bir geçerli bir tarih değil.<br>";
var $err_bitis_nodate = "Bitiş bir geçerli bir tarih değil.<br>";

var $err_valid_tarih = "Bitiş tarihi başlangıç tarihinden daha erken.<br>";
$modal_message.html('');
var $form_items = [$baslangic, $bitis];
var $err_contents = [$err_baslangic, $err_bitis];
var $err_contents_nodate = [$err_baslangic_nodate, $err_bitis_nodate];
var $valid_tarih;
var $valid_is_date;
$tamamla_btn.click(function () {
  $valid_tarih = true;
  $valid_is_date = true;
  //Girdilerin dolu olduğunu kontrol eden bölüm.
  $modal_message.html('');
  for (var $i = 0; $i < $form_items.length; $i++) {
    if ($form_items[$i].val() == '' && $modal_message.html().search($err_contents[$i]) == -1) {
      $form_items[$i].next(".input-req").removeClass("input-req-success");
      $form_items[$i].next(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_contents[$i]);
    } else if ($form_items[$i].val() != '' && $modal_message.html().search($err_contents[$i]) != -1) {
      $form_items[$i].next(".input-req").addClass("input-req-success");
      $form_items[$i].next(".input-req").removeClass("input-req-fail");
      $modal_message.html($modal_message.html().replace($err_contents[$i], ""));
    }
  }

  if ($baslangic.val() != '' && $bitis.val() != '') {
    console.log("enter");
    for (var $i = 0; $i < $form_items.length; $i++) {
      if (!Date.parse($form_items[$i].val()) && $modal_message.html().search($err_contents_nodate[$i]) == -1) {
        $form_items[$i].next(".input-req").removeClass("input-req-success");
        $form_items[$i].next(".input-req").addClass("input-req-fail");
        $modal_message.html($modal_message.html() + $err_contents_nodate[$i]);
        $valid_is_date = false;
      } else if (Date.parse($form_items[$i].val()) && $modal_message.html().search($err_contents_nodate[$i]) != -1) {
        $form_items[$i].next(".input-req").addClass("input-req-success");
        $form_items[$i].next(".input-req").removeClass("input-req-fail");
        $modal_message.html($modal_message.html().replace($err_contents_nodate[$i], ""));
        $valid_is_date = true;
      }
    }
    console.log($valid_is_date);
  }

  //E-posta adresinin geçerli olup olmadığını kontrol eden bölüm.
  var $baslangic_date = new Date($baslangic.val());
  var $bitis_date = new Date(new Date($bitis.val()));
  if ($baslangic.val() != '' && $bitis.val() != '' && $valid_is_date) {
    console.log($baslangic_date + " " + $bitis_date);
    if ($baslangic_date > $bitis_date) {
      $valid_tarih = false;
      $modal_message.html($modal_message.html() + $err_valid_tarih);
    }
  }

  //Modal'ı gizleyen veya gösteren bölüm.(Titreme efekti ile.)
  if ($modal_message.html() != '') {
    $modal.css("display", "block");
    $modal.removeClass("vibrate");
    setTimeout('$modal.addClass("vibrate");', 100);
  }
  else {
    $modal.css("display", "none");
  }

});

//Modal'ı kapatan buton için click handler.
$close_btn.click(function () {
  $modal.css("display", "none");
});

$tamamla_btn.click(function () {
  $("#form").submit();
});

$("input").focus(function () {
  $(this).next(".input-req").removeClass("input-req-fail");
  $(this).next(".input-req").addClass("input-req-success");
});

//Girdilerden biri boşsa submit işlemini iptal eden fonksyon.
function tamamla_valid() {
  if ($baslangic.val() == '' || $bitis.val() == '' || !$valid_tarih || !$valid_is_date) {
    return false;
  }
}