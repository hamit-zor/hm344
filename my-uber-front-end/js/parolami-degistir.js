//Modal, girdiler ve hata mesajları tanımlanıyor.
var $modal = $(".modal");
var $modal_message = $(".modal-message");

var $close_btn = $(".btn-modal-close");
var $kayit_btn = $("#btn-parola-degistir");

var $password_old = $("#password-old");
var $password = $("#password");
var $password_again = $("#password-again");
var $valid_password_again = false;

var $err_password_old = "Eski parola boş bırakılamaz<br>";
var $err_password = "Yeni parola boş bırakılamaz!<br>";
var $err_password_again = "Yeni parola tekrarı boş bırakılamaz!<br>";

var $err_valid_password_again = "Girilen parolalar eşleşmemektedir.<br>";
$modal_message.html('');
var $form_items = [$password_old, $password, $password_again];
var $err_contents = [$err_password_old, $err_password, $err_password_again];

$kayit_btn.click(function () {
  $valid_password_again = true;
  //Girdilerin dolu olduğunu kontrol eden bölüm.
  $modal_message.html('');
  for (var $i = 0; $i < $form_items.length; $i++) {
    if ($form_items[$i].val() == '' && $modal_message.html().search($err_contents[$i]) == -1) {
      $form_items[$i].siblings(".input-req").removeClass("input-req-success");
      $form_items[$i].siblings(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_contents[$i]);
    } else if ($form_items[$i].val() != '' && $modal_message.html().search($err_contents[$i]) != -1) {
      $form_items[$i].siblings(".input-req").addClass("input-req-success");
      $form_items[$i].siblings(".input-req").removeClass("input-req-fail");
      $modal_message.html($modal_message.html().replace($err_contents[$i], ""));
    }
  }

  //Parolaların eşleşip eşleşmediğini kontrol eden bölüm
  if ($password.val() != '' && $password_again.val() != '') {
    if ($password.val() != $password_again.val()) {
      $valid_password_again = false;
      $password_again.siblings(".input-req").removeClass("input-req-success");
      $password_again.siblings(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_valid_password_again);
    }
  }


  //Modal'ı gizleyen veya gösteren bölüm.(Titreme efekti ile.)
  if ($modal_message.html() != '') {
    $modal.css("display", "block");
    $modal.removeClass("vibrate");
    setTimeout('$modal.addClass("vibrate");', 100);
  }
  else {
    $modal.css("display", "none");
  }

});

//Modal'ı kapatan buton için click handler.
$close_btn.click(function () {
  $modal.css("display", "none");
});

$kayit_btn.click(function () {
  $("#form").submit();
});

$("input").focus(function () {
  $(this).siblings(".input-req").removeClass("input-req-fail");
  $(this).siblings(".input-req").addClass("input-req-success");
});

//Girdilerden biri boşsa submit işlemini iptal eden fonksyon.
function tamamla_valid() {
  console.log($valid_password_again);
  if ($password.val() == '' || $password_old.val() == '' || $password_again.val() == '' || !$valid_password_again) {
    return false;
  }
}