-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- şemanın yaratılması
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create database myuber default character set utf8 default collate utf8_turkish_ci;
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- tabloların yaratılması
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create table il(
   il_id int not null auto_increment,
   il_isim varchar(50) not null,
   il_baslangic_fiyat int not null,
   primary key(il_id),
   constraint il_isim_unique unique (il_isim)
);
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create table kullanici(
   kullanici_id int not null auto_increment,
   kullanici_ad varchar(50) not null,
   kullanici_soyad varchar(50) not null,
   kullanici_eposta varchar(50) not null,
   kullanici_parola char(64) not null,
   primary key(kullanici_id),
   constraint kullanici_eposta_unique unique (kullanici_eposta)
);
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create table sofor(
   sofor_id int not null auto_increment,
   sofor_ad varchar(50) not null,
   sofor_soyad varchar(50) not null,
   sofor_eposta varchar(50) not null,
   sofor_parola char(64) not null,
   primary key(sofor_id),
   constraint sofor_eposta_unique unique (sofor_eposta)
);
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create table yolculuk(
   yolculuk_id int not null auto_increment,
   yolculuk_kalkis_yeri varchar(50) not null,
   yolculuk_varis_yeri varchar(50) not null,
   yolculuk_tarih date not null,
   yolculuk_mesafe int not null,
   yolculuk_fiyat int not null,
   yolculuk_il_id int not null,
   yolculuk_kullanici_id int not null,
   yolculuk_sofor_id int not null,
   primary key(yolculuk_id),
   foreign key (yolculuk_il_id) references il(il_id) on delete cascade,
   foreign key (yolculuk_kullanici_id) references kullanici(kullanici_id) on delete cascade,
   foreign key (yolculuk_sofor_id) references sofor(sofor_id) on delete cascade
);
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- triggerların yaratılması
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
create trigger yolculuk_fiyat_hesapla before insert on yolculuk
for each row set new.yolculuk_fiyat = (
    select il.il_baslangic_fiyat 
    from il
	where il.il_id=new.yolculuk_il_id)+
    2*(new.yolculuk_mesafe);


-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- stored procedure lerin yaratılması
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
delimiter //
create procedure yolculuklari_getir 
(in kullanici_id_in int)
begin
  select yolculuk.yolculuk_id,yolculuk.yolculuk_kalkis_yeri,yolculuk.yolculuk_varis_yeri,yolculuk.yolculuk_tarih,yolculuk.yolculuk_mesafe,yolculuk.yolculuk_fiyat,
  il.il_id,il.il_isim,il.il_baslangic_fiyat,sofor.sofor_id,sofor.sofor_ad,sofor.sofor_soyad,sofor.sofor_eposta
  from yolculuk,il,sofor 
  where yolculuk.yolculuk_kullanici_id = kullanici_id_in
  and
  yolculuk.yolculuk_il_id=il.il_id
  and
  yolculuk.yolculuk_sofor_id=sofor.sofor_id
  order by yolculuk.yolculuk_tarih;
end //
delimiter ;
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
delimiter //
create procedure tarihe_gore_yolculuklari_getir 
(in kullanici_id_query int,in baslangic_tarih date,in bitis_tarih date)
begin
  select yolculuk.yolculuk_id,yolculuk.yolculuk_kalkis_yeri,yolculuk.yolculuk_varis_yeri,yolculuk.yolculuk_tarih,yolculuk.yolculuk_mesafe,yolculuk.yolculuk_fiyat,
  il.il_id,il.il_isim,il.il_baslangic_fiyat,sofor.sofor_id,sofor.sofor_ad,sofor.sofor_soyad,sofor.sofor_eposta,SUM(yolculuk.yolculuk_mesafe) as toplam_mesafe,SUM(yolculuk.yolculuk_fiyat) as toplam_fiyat
  from yolculuk,il,sofor,kullanici
  where yolculuk.yolculuk_il_id=il.il_id
  and
  yolculuk.yolculuk_sofor_id=sofor.sofor_id
  and ((yolculuk.yolculuk_tarih between baslangic_tarih and bitis_tarih) and (yolculuk.yolculuk_kullanici_id=kullanici.kullanici_id) and kullanici.kullanici_id=kullanici_id_query)
  order by yolculuk.yolculuk_tarih;
end //
delimiter ;
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
delimiter //
create procedure sofor_getir 
(in yolculuk_id_in int)
begin
  select sofor.sofor_ad,sofor.sofor_soyad,sofor.sofor_eposta
  from sofor
  where sofor.sofor_id = (select yolculuk.yolculuk_sofor_id from yolculuk where yolculuk_id=yolculuk_id_in);
end //
delimiter ;
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
delimiter //
create procedure yolculuk_detay_getir 
(in yolculuk_id_in int)
begin
  select yolculuk.yolculuk_id,yolculuk.yolculuk_kalkis_yeri,yolculuk.yolculuk_varis_yeri,yolculuk.yolculuk_tarih,yolculuk.yolculuk_mesafe,yolculuk.yolculuk_fiyat,
  il.il_id,il.il_isim,il.il_baslangic_fiyat,sofor.sofor_id,sofor.sofor_ad,sofor.sofor_soyad,sofor.sofor_eposta
  from yolculuk,il,sofor 
  where yolculuk.yolculuk_il_id=il.il_id
  and
  yolculuk.yolculuk_sofor_id=sofor.sofor_id
  and
  yolculuk.yolculuk_id = yolculuk_id_in
  order by yolculuk.yolculuk_tarih;
end //
delimiter ;
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- test için veri eklenmesi
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into `il` (`il_isim`, `il_baslangic_fiyat`) values
('Ankara', 7),
('İstanbul', 10),
('Bursa', 5),
('İzmir', 7),
('Adana', 4),
('Antalya', 4),
('Diyarbakır', 3),
('Trabzon', 7);
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into `kullanici` (`kullanici_ad`, `kullanici_soyad`, `kullanici_eposta`, `kullanici_parola`) values
('Hamit', 'Zor (administrator)', 'thenrerise@gmail.com', '4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2'),
('Selim', 'Işık', 'selim@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Bora', 'Utay', 'bora@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Osman', 'Tarak', 'osman@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Kerem', 'Oluk', 'kerem@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Faruk', 'Tantan', 'faruk@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into `sofor` (`sofor_ad`, `sofor_soyad`, `sofor_eposta`, `sofor_parola`) values
('Tugay', 'Kuyucu', 'tugay@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Utku', 'Hakan', 'utku@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Murat', 'Eren', 'murat@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Mücahit', 'Erdem', 'mücahit@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Enes', 'Paslı', 'enes@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Hakan', 'Taşcı', 'hakan@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Turgut', 'Kilimci', 'turgut@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Savaş', 'Onur', 'savas@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Harun', 'Dalçeşme', 'harun@umail.com', 'ad8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918min'),
('Alp', 'Yumuşak', 'alp@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Abdullah', 'Ilıkcı', 'abdullah@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'),
('Umut', 'Çatlı', 'umut@umail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918');
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into `yolculuk` (`yolculuk_kalkis_yeri`, `yolculuk_varis_yeri`, `yolculuk_tarih`, `yolculuk_mesafe`, `yolculuk_fiyat`, `yolculuk_il_id`, `yolculuk_kullanici_id`, `yolculuk_sofor_id`) values
('Haymana ', 'Kazan ', '2014-01-07', 60, 127, 1, 1, 3),
('Ayaş ', 'Çankaya ', '2015-02-27', 45, 97, 1, 1, 5),
('Etimesgut ', 'Keçiören ', '2015-07-08', 43, 93, 1, 1, 6),
('Kızılcahamam ', 'Nallıhan ', '2016-05-18', 72, 151, 1, 1, 2),
('Polatlı ', 'Gölbaşı ', '2017-12-13', 81, 169, 1, 1, 3),
('Çatalca ', 'Esenyurt ', '2017-04-12', 41, 92, 2, 1, 4),
('Fatih ', 'Beşiktaş ', '2017-01-02', 21, 52, 2, 1, 9),
('Bağcılar ', 'Bayrampaşa ', '2017-03-07', 11, 32, 2, 1, 8),
('Bergama ', 'Çiğli ', '2013-06-04', 21, 49, 4, 2, 3),
('Güzelbahçe ', 'Karaburun ', '2013-07-17', 13, 33, 4, 2, 7),
('Kemalpaşa ', 'Konak ', '2013-12-25', 41, 89, 4, 2, 2),
('Nilüfer ', 'Kestel ', '2013-12-30', 12, 29, 3, 2, 8),
('Osmangazi ', 'Orhangazi ', '2014-12-17', 22, 49, 3, 2, 7),
('Narlıdere ', 'Selçuk ', '2016-12-30', 23, 53, 4, 2, 10),
('Güzelbahçe ', 'Aliağa ', '2016-02-16', 44, 95, 4, 2, 11),
('Beydağ ', 'Buca ', '2017-06-13', 51, 109, 4, 2, 9),
('Bergama ', 'Kiraz ', '2018-06-12', 10, 27, 4, 2, 10),
('Kemalpaşa ', 'Kiraz ', '2018-05-14', 61, 129, 4, 2, 8),
('Gaziemir ', 'Güzelbahçe ', '2018-05-28', 33, 73, 4, 2, 6),
('Dikili ', 'Foça ', '2018-05-31', 6, 19, 4, 2, 4),
('Ceyhan ', 'Feke ', '2017-02-14', 12, 28, 5, 3, 3),
('İmamoğlu ', 'Karataş ', '2017-03-15', 20, 44, 5, 3, 5),
('Kozan ', 'Pozantı ', '2017-03-23', 13, 30, 5, 3, 5),
('Sarıçam ', 'Seyhan ', '2017-11-21', 41, 86, 5, 3, 7),
('Feke ', 'Sarıçam ', '2017-12-28', 55, 114, 5, 3, 7),
('Karaisalı ', 'Pozantı ', '2018-05-15', 23, 50, 5, 3, 7),
('Tufanbeyli ', 'Sarıçam ', '2018-02-13', 24, 52, 5, 3, 8),
('Yumurtalık ', 'Yüreğir ', '2018-04-18', 26, 56, 5, 3, 9),
('İmamoğlu', 'Kozan ', '2018-10-17', 61, 126, 5, 3, 9),
('Kayapınar ', 'Kocaköy ', '2017-12-13', 21, 45, 7, 4, 1),
('Dicle ', 'Kayapınar ', '2018-01-16', 72, 147, 7, 4, 6),
('Kocaköy ', 'Silvan ', '2018-02-20', 61, 125, 7, 4, 7),
('Çüngüş ', 'Çınar ', '2018-06-21', 15, 33, 7, 4, 4),
('Ergani ', 'Lice ', '2018-08-28', 25, 53, 7, 4, 10),
('Kepez ', 'Döşemealtı ', '2018-05-31', 26, 56, 6, 4, 6),
('Kemer ', 'Korkuteli ', '2018-04-19', 51, 106, 6, 4, 8),
('Gazipaşa ', 'Aksu ', '2018-04-26', 41, 86, 6, 4, 6),
('Hayrat ', 'Maçka ', '2018-05-01', 23, 53, 8, 5, 6),
('Düzköy ', 'Arsin ', '2018-05-23', 17, 41, 8, 5, 8),
('Dernekpazarı ', 'Of ', '2018-05-31', 15, 37, 8, 5, 12);
-- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
