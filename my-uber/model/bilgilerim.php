<?php

function get_user_data($email) {
  include './model/conn.php';
  $query = "select * from kullanici where kullanici_eposta = '$email'";
  $result = mysqli_query($connect, $query);
  $result_row_count = mysqli_num_rows($result);

  if ($result_row_count > 0) {
    $row = mysqli_fetch_assoc($result);
  } else {
    $row = NULL;
  }

  mysqli_close($connect);
  return $row;
}

function set_user_data($name, $sirname, $email,$id) {
  include './model/conn.php';
  
  $sql = "update kullanici set kullanici_ad='".$name."',kullanici_soyad='".$sirname."',kullanici_eposta='".$email."' where kullanici_id=".$id;

  if ($connect->query($sql) === TRUE) {
    return "Bilgileriniz başarılı şekilde güncellendi.";
  } else {
    return "Bilgilerinizi güncellerken hata oluştu : " . $connect->error;
  }
}

function set_password($old_password, $new_password, $id){
  include './model/conn.php';
  
  $new_password=hash('sha256', $new_password);
  
  $sql = "update kullanici set kullanici_parola='".$new_password."' where kullanici_id=".$id;

  if ($connect->query($sql) === TRUE) {
    return "Parolanız başarılı şekilde güncellendi.";
  } else {
    return "Parolanızı güncellerken hata oluştu : " . $connect->error;
  }
}

function kullanici_bilgisini_al($email) {
  include './model/conn.php';
  $query = "select * from kullanici where kullanici_eposta = '$email'";
  $result = mysqli_query($connect, $query);
  $result_row_count = mysqli_num_rows($result);

  if ($result_row_count > 0) {
    $row = mysqli_fetch_assoc($result);
  } else {
    $row = NULL;
  }
  
  mysqli_close($connect);
  return $row;
}

?>