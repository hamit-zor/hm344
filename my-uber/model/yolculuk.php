<?php

function get_user_data($email) {
  include './model/conn.php';
  $query = "select * from kullanici where kullanici_eposta = '$email'";
  $result = mysqli_query($connect, $query);
  $result_row_count = mysqli_num_rows($result);

  if ($result_row_count > 0) {
    $row = mysqli_fetch_assoc($result);
  } else {
    $row = NULL;
  }

  mysqli_close($connect);
  return $row;
}

function get_yolculuklar($id) {
  include './model/conn.php';

  $query = 'call yolculuklari_getir(' . $id . ');';
  $result = mysqli_query($connect, $query);
  $result_row_count = mysqli_num_rows($result);
  $i = 1;

  if ($result_row_count < 1) {
    return NULL;
  }


  while ($row = mysqli_fetch_assoc($result)) {
    $row_set[$i++] = $row;
  }

  mysqli_close($connect);
  return $row_set;
}

function yolculuk_ekle($tarih, $kalkis, $varis, $mesafe, $il, $sofor, $id) {
  include './model/conn.php';

  $kelimeler = explode(' ', trim($sofor));
  $sofor_ad = $kelimeler[0];
  $sofor_soyad = array_pop($kelimeler);

  $query_il = 'select * from il where il_isim=\'' . $il . '\';';
  $query_sofor = 'select * from sofor where sofor_ad=\'' . $sofor_ad . '\' and sofor_soyad=\'' . $sofor_soyad . '\';';
  $result_il = mysqli_query($connect, $query_il);
  $result_sofor = mysqli_query($connect, $query_sofor);
  $data_il = mysqli_fetch_assoc($result_il);
  $data_sofor = mysqli_fetch_assoc($result_sofor);
  $result_row_count_il = mysqli_num_rows($result_il);
  $result_row_count_sofor = mysqli_num_rows($result_sofor);


  if ($result_row_count_il < 1 or $result_row_count_sofor < 1) {
    return "Yolculuk ekleme işlemi sırasında bir hata oluştu : İl veya şoför ismi hatalıdır." . $query_il . $query_sofor;
  }

  $il_id = $data_il['il_id'];
  $sofor_id = $data_sofor['sofor_id'];

  $sql = "insert into yolculuk(yolculuk_tarih,yolculuk_kalkis_yeri,yolculuk_varis_yeri,yolculuk_mesafe,yolculuk_fiyat,yolculuk_il_id,yolculuk_kullanici_id,yolculuk_sofor_id)
          values 
          ('" . $tarih . "','" . $kalkis . "','" . $varis . "','" . $mesafe . "','1','" . $il_id . "','" . $id . "','" . $sofor_id . "');";

  if ($connect->query($sql) === TRUE) {
    return "Yolculuk ekleme işlemi başarılı bir şekilde gerçekleşti.";
  } else {
    return "Yolculuk ekleme işlemi sırasında bir hata oluştu : " . $id . $sql . $connect->error;
  }
}

function get_soforler() {
  include './model/conn.php';
  $sql = "select * from sofor";

  $result = $connect->query($sql);
  $row_set = array();
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      array_push($row_set, $row);
    }
  } else {
    return NULL;
  }
  $connect->close();
  return $row_set;
}

function get_iller() {
  include './model/conn.php';
  $sql = "select * from il";

  $result = $connect->query($sql);
  $row_set = array();
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      array_push($row_set, $row);
    }
  } else {
    return NULL;
  }
  $connect->close();
  return $row_set;
}

function get_sofor($yolculuk_id) {
  include './model/conn.php';

  $query = "call sofor_getir(" . $yolculuk_id . ");";

  $result = mysqli_query($connect, $query);

  $result_row_count = mysqli_num_rows($result);

  if ($result_row_count > 0) {
    $row = mysqli_fetch_assoc($result);
  } else {
    $row = NULL;
  }
  mysqli_close($connect);
  return $row;
}

function get_yolculuk_detay($yolculuk_id) {
  include './model/conn.php';

  $query = "call yolculuk_detay_getir(" . $yolculuk_id . ");";

  $result = mysqli_query($connect, $query);

  $result_row_count = mysqli_num_rows($result);

  if ($result_row_count > 0) {
    $row = mysqli_fetch_assoc($result);
  } else {
    $row = NULL;
  }
  mysqli_close($connect);
  return $row;
}

function get_filtrelenmis_yolculuklar($id, $baslangic, $bitis) {
  include './model/conn.php';

  $query = "call tarihe_gore_yolculuklari_getir(" . $id . ",'" . $baslangic . "','" . $bitis . "');";

  $result = mysqli_query($connect, $query);
  $result_row_count = mysqli_num_rows($result);
  $i = 1;

  if ($result_row_count < 1) {
    return NULL;
  }


  while ($row = mysqli_fetch_assoc($result)) {
    $row_set[$i++] = $row;
  }

  mysqli_close($connect);
  return $row_set;
}

?>