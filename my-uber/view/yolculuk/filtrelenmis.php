<div class="wrapper header">
  <div class="h1">
    <a href="./?url=anasayfa">MyUber</a>
  </div>
  <div class="display-flex flex-row justify-content-between bar">
    <div>
      <a href="./?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
      <a href="./?url=yolculuklarim" class="button">Yolculuklarım</a>
    </div>
    <div>
      <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
    </div>
  </div>
  <div class="panel">
    <div class="panel-title"><span class="date"><?php echo $model['baslangic']; ?></span> ile <span class="date"><?php echo $model['bitis']; ?></span> arası yolculuklar</div>
    <div class="panel-content">
      <div class="<?php echo $model['yolculuk_aciklama_css']; ?>"><?php echo $model['yolculuk_aciklama']; ?></div>
      <div class="aciklamalar">
        <span class="tarih">Tarih</span>
        <span class="kalis-varis">Kalkış-Varış</span>
        <span class="mesafe">Mesafe</span>
        <span class="fiyat">Fiyat</span>
        <span class="il">İl</span>
        <hr class="hr">
      </div>
      <?php
      $toplam_fiyat=0;
      $toplam_mesafe=0;
      foreach ($model['yolculuklar'] as $row) {
        echo '<div class="form-l-i">
            <a class="button button-yolculuk" href="./?url=yolculuklarim/detay&id=' . $row['yolculuk_id'] . '">
              <div class="card-detail">
                <span class="tarih">' . $row['yolculuk_tarih'] . '</span>
                <span class="kalis-varis">' . $row['yolculuk_kalkis_yeri'] . '-' . $row['yolculuk_varis_yeri'] . '</span>
                <span class="mesafe">' . $row['yolculuk_mesafe'] . ' km</span>
                <span class="fiyat">' . $row['yolculuk_fiyat'] . ' TL</span>
                <span class="il">' . $row['il_isim'] . '</span>
              </div>
            </a>
          </div>';
        $toplam_fiyat = $toplam_fiyat+$row['yolculuk_fiyat'];
        $toplam_mesafe = $toplam_mesafe + $row['yolculuk_mesafe'];
      }
      ?>
      <div class="toplam-bilgileri display-flex flex-column align-items-end">
        <div>
          Toplam Fiyat:<?php echo '  '.$toplam_fiyat.' TL'?>
        </div>
        <div>
        Toplam Katedilen Mesafe:<?php echo '  '.$toplam_mesafe.' km'?>
      </div>
      </div>
      
    </div>
  </div>
</div>