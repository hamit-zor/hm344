<div class="wrapper header">
  <div class="h1">
    <a href="?url=anasayfa-giris-yapildi">MyUber</a>
  </div>
  <div class="display-flex flex-row justify-content-between bar">
    <div>
      <a href="?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
      <a href="?url=yolculuklarim" class="button">Yolculuklarım</a>
    </div>
    <div>
      <a href="?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
    </div>
  </div>
  <div class="panel">
    <div class="panel-title">Yolculuk Detayları</div>
    <div class="panel-content">
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Tarih</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="tarih" type="text" readonly value="<?php echo $model['yolculuk_tarih']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Kalkış Yeri</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="kalkis" type="text" readonly value="<?php echo $model['yolculuk_kalkis_yeri']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Varış Yeri</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="varis" type="text" readonly value="<?php echo $model['yolculuk_varis_yeri']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Mesafe</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="mesafe" type="text" readonly value="<?php echo $model['yolculuk_mesafe']; ?> km">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Fiyat</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="fiyat" type="text" readonly value="<?php echo $model['yolculuk_fiyat']; ?> TL">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>İl</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="il" type="text" readonly value="<?php echo $model['il_isim']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Şoför</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="sofor" type="text" readonly value="<?php echo $model['sofor_ad'] . ' ' . $model['sofor_soyad'] ?>">
          </div>
        </div>
      </div>
    </div>

    <div class="form-b">
      <div>
        <a href="./?url=yolculuklarim/sofor-bilgileri&id=<?php echo $model['yolculuk_id'];?>" id="btn-login" class="button">Şoför bilgilerine git</a>
      </div>
    </div>
  </div>
</div>