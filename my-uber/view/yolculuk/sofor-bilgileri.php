<div class="wrapper header">
  <div class="h1">
    <a href="./?url=anasayfa">MyUber</a>
  </div>
  <div class="display-flex flex-row justify-content-between bar">
    <div>
      <a href="./?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
      <a href="./?url=yolculuklarim" class="button">Yolculuklarım</a>
    </div>
    <div>
      <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
    </div>
  </div>
  <div class="panel">
    <div class="panel-title">Şoför bilgileri</div>
    <div class="panel-content">
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Şoför adı</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="name" type="text" readonly value="<?php echo $model['sofor_ad']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Şoför Soyadı</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="sirname" type="text" readonly value="<?php echo $model['sofor_soyad']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Şoför E-posta adresi</div>
          </div>
        </div>
        <div class="content-data-email">
          <div class="form-l-i">
            <input id="email" type="text" readonly value="<?php echo $model['sofor_eposta']; ?>">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>