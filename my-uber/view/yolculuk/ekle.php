<input id="ekle_success_container" type="hidden" value="<?php echo $model['ekle_success'] ?>">
<div class="modal">
  <div id="modal" class="modal-content">
    <div class="btn-modal-close">
      <i class="fa fa-times-circle"></i>
    </div>
    <div class="modal-message">
    </div>
  </div>
</div>
<form id="form" method="post" action="?url=ekle-denemesi" onsubmit="return kayit_valid()">
  <div class="wrapper header">
    <div class="h1">
      <a href="?url=anasayfa-giris-yapildi">MyUber</a>
    </div>
    <div class="display-flex flex-row justify-content-between bar">
      <div>
        <a href="?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
        <a href="?url=yolculuklarim" class="button">Yolculuklarım</a>
      </div>
      <div>
        <a href="?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
      </div>
    </div>
    <div class="panel">
      <div class="panel-title">Yolculuk Ekle</div>
      <div class="form-l-i">
        <div>Tarih</div>
        <div>
          <input name="tarih" id="tarih" type="date" value="">
          <i class="fa fa-times input-req input-req-success"></i>
        </div>
      </div>
      <div class="form-l-i">
        <div>Kalkış yeri</div>
        <div>
          <input name="kalkis" id="kalkis" type="text" value="">
          <i class="fa fa-times input-req input-req-success"></i>
        </div>
      </div>
      <div class="form-l-i">
        <div>Varış yeri</div>
        <div>
          <input name="varis" id="varis" type="text" value="">
          <i class="fa fa-times input-req input-req-success"></i>
        </div>
      </div>
      <div class="form-ps">
        <div class="form-l-i">
          <div>Mesafe(km)</div>
          <div>
            <input name="mesafe" id="mesafe" type="number" value="">
            <i class="fa fa-times input-req input-req-success"></i>
          </div>
        </div>
        <div class="form-l-i">
          <div>İl</div>
          <div>
            <select name="il" id="il">
              <?php 
              foreach ($model['iller'] as $value) {
                echo '<option>'.$value['il_isim'].'</option>';
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-l-i">
          <div>Şoför</div>
          <div>
            <select name="sofor" id="sofor">
              <?php 
              foreach ($model['soforler'] as $value) {
                echo '<option>'.$value['sofor_ad'].' '.$value['sofor_soyad'].'</option>';
              }
              ?>
            </select>
          </div>
        </div>
      </div>
      <div class="form-b">
        <div>
          <a id="btn-yolculuk-ekle" class="button">Yolculuk Ekle</a>
        </div>
      </div>
    </div>
  </div>
</form>