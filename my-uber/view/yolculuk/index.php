<input id="ekle_success_container" type="hidden" value="<?php echo $model['ekle_success'] ?>">
<div class="modal">
  <div id="modal" class="modal-content">
    <div class="btn-modal-close">
      <i class="fa fa-times-circle"></i>
    </div>
    <div class="modal-message">
    </div>
  </div>
</div>
<form id="form" method="post" action="./?url=yolculuklarim/filtrelenmis" onsubmit="return tamamla_valid()">
  <div class="wrapper header">
    <a class="h1" href="./?url=anasayfa">MyUber</a>
    <div class="display-flex flex-row justify-content-between bar">
      <div>
        <a href="./?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
        <a href="./?url=yolculuklarim" class="button">Yolculuklarım</a>
      </div>
      <div>
        <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
      </div>
    </div>
    <div class="panel">
      <div class="panel-title">Yolculuklarım</div>
      <div class="panel-content">
        <div class="<?php echo $model['yolculuk_aciklama_css']; ?>"><?php echo $model['yolculuk_aciklama']; ?></div>
        <div class="aciklamalar">
          <span class="tarih">Tarih</span>
          <span class="kalis-varis">Kalkış-Varış</span>
          <span class="mesafe">Mesafe</span>
          <span class="fiyat">Fiyat</span>
          <span class="il">İl</span>
          <hr class="hr">
        </div>
        <?php
        foreach ($model['yolculuklar'] as $row) {
          echo '<div class="form-l-i">
            <a class="button button-yolculuk" href="./?url=yolculuklarim/detay&id=' . $row['yolculuk_id'] . '">
              <div class="card-detail">
                <span class="tarih">' . $row['yolculuk_tarih'] . '</span>
                <span class="kalis-varis">' . $row['yolculuk_kalkis_yeri'] . '- ' . $row['yolculuk_varis_yeri'] . '</span>
                <span class="mesafe">' . $row['yolculuk_mesafe'] . ' km</span>
                <span class="fiyat">' . $row['yolculuk_fiyat'] . ' TL</span>
                <span class="il">' . $row['il_isim'] . '</span>
              </div>
            </a>
          </div>';
        }
        ?>
      </div>
      <div class="form-b">
        <div class="display-flex flex-row justify-content-between">
          <div class="form-l-i">
            <div>
              <span class="filtre-ad">Başlangıç tarihi</span>
              <span>Bitiş tarihi</span>
            </div>
            <div>
              <input class="date" name="baslangic" id="baslangic" type="date">
              <i class="fa fa-times input-req input-req-success"></i>
              <input class="date" name="bitis" id="bitis" type="date" placeholder="">
              <i class="fa fa-times input-req input-req-success"></i>
              <a id="btn-filtrele" class="button">Tarihe göre filtrele</a>
              <a href="?url=yolculuklarim/ekle" id="btn-yolculuk-ekle" class="button">Yolculuk ekle</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>