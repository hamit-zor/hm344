<input id="kayit_success_container" type="hidden" value="<?php echo $model['kayit_success'] ?>">
<div class="modal">
  <div class="modal-content">
    <div class="btn-modal-close">
      <i class="fa fa-times-circle"></i>
    </div>
    <div class="modal-message">
    </div>
  </div>
</div>
<form id="form" method="POST" action="?url=kayit-denemesi" onsubmit="return kayit_valid()">
    <div class="wrapper header">
      <div class="h1">
        <a href="?url=anasayfa">MyUber</a>
      </div>
      <div class="display-flex flex-row justify-content-end bar">
        <a href="?url=giris" class="button">Giriş Yap</a>
      </div>
      <div class="panel">
        <div class="panel-title">Kayıt Ol</div>
        <div class="form-l-i">
          <div>Ad*</div>
          <div>
            <input name="name" id="name" type="text">
            <i class="fa fa-times input-req input-req-success"></i>
          </div>
        </div>
        <div class="form-l-i">
          <div>Soyad*</div>
          <div>
            <input name="sirname" id="sirname" type="text">
            <i class="fa fa-times input-req input-req-success"></i>
          </div>
        </div>
        <div class="form-l-i">
          <div>E-posta adresi*</div>
          <div>
            <input name="email" id="email" type="email">
            <i class="fa fa-times input-req input-req-success"></i>
          </div>
        </div>
        <div class="form-ps">
          <div class="form-l-i">
            <div>Parola*</div>
            <div>
              <input name="password" id="password" type="password">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
          <div class="form-l-i">
            <div>Parolayı tekrar girin</div>
            <div>
              <input name="password-again" id="password-again" type="password">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
        <div class="form-b">
          <div>
            <a id="btn-kayit" class="button">Kayıt Ol</a>
          </div>
        </div>
      </div>
    </div>
  </form>