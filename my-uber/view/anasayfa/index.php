<div class="wrapper header">
    <div class="h1">
      <a href="?url=anasayfa">MyUber</a>
    </div>
    <div class="display-flex flex-row justify-content-end bar">
      <a href="?url=kayit" class="button">Kayıt Ol</a>
      <a href="?url=giris" class="button">Giriş Yap</a>
    </div>
    <div class="karsilama">
      <div>MyUber'e</div>
      <div class="buyuk">Hoşgeldiniz</div>
      <div class="kucuk">Lütfen giriş yapınız</div>
    </div>
  </div>