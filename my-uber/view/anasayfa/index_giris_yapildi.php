<div class="wrapper header">
    <div class="h1">
      <a href="?url=anasayfa">MyUber</a>
    </div>
    <div class="display-flex flex-row justify-content-between bar">
      <div>
        <a href="?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
        <a href="?url=yolculuklarim" class="button">Yolculuklarım</a>
      </div>
      <div>
        <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
      </div>
    </div>
    <div class="karsilama">
      <div>MyUber'e</div>
      <div class="buyuk">Hoşgeldiniz</div>
      <div class="kucuk">Sn. <?php echo $model['user_name']." ".$model['user_sirname']; ?></div>
    </div>
  </div>