<input id="kayit_success_container" type="hidden" value="<?php echo $model['kayit_success'] ?>">
<input id="login_success_container" type="hidden" value="<?php echo $model['login_success'] ?>">

<form id="form" method="POST" action="?url=oturum-acma-denemesi" onsubmit="return login_valid()">
  <div class="modal">
    <div id="modal" class="modal-content">
      <div class="btn-modal-close">
        <i class="fa fa-times-circle"></i>
      </div>
      <div class="modal-message">
      </div>
    </div>
  </div>
  <div class="wrapper header">
    <div class="h1">
      <a href="?url=anasayfa">MyUber</a>
    </div>
    <div class="display-flex flex-row justify-content-end bar">
      <a href="?url=kayit" class="button">Kayıt Ol</a>
    </div>
    <div class="panel">
      <div class="panel-title">Giriş Yap</div>
      <div class="form-l-i">
        <div>E-posta adresi</div>
        <div>
          <input name="email" id="email" type="email" value="<?php echo $model['email_attempt'] ?>">
          <i class="fa fa-times input-req input-req-success"></i>
        </div>
      </div>
      <div class="form-l-i">
        <div>Parola</div>
        <div>
          <input name="password" id="password" type="password">
          <i class="fa fa-times input-req input-req-success"></i>
        </div>
      </div>
      <div class="form-b">
        <div>
          <a id="btn-login" class="button">Giriş Yap</a>
        </div>
      </div>
    </div>
  </div>
</form>