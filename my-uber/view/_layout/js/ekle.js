//Modal, girdiler ve hata mesajları tanımlanıyor.
var $modal = $(".modal");
var $modal_message = $(".modal-message");

var $close_btn = $(".btn-modal-close");
var $kayit_btn = $("#btn-yolculuk-ekle");

var $tarih = $("#tarih");
var $kalkis = $("#kalkis");
var $varis = $("#varis");
var $mesafe = $("#mesafe");
var $valid_mesafe = false;
var $ekle_success_container = $("#ekle_success_container");

var $err_tarih = "Tarih boş bırakılamaz!<br>";
var $err_kalkis = "Kalkış yeri boş bırakılamaz!<br>";
var $err_varis = "Varış yeri boş bırakılamaz!<br>";
var $err_mesafe = "Mesafe boş bırakılamaz!<br>";

var $err_valid_mesafe = "Girilen mesafe çok fazladır.<br>";

$modal_message.html('');

var $form_items = [$tarih, $kalkis, $varis, $mesafe];
var $err_contents = [$err_tarih, $err_kalkis, $err_varis, $err_mesafe];

$(document).ready(function () {
  if ($ekle_success_container.val() != 'Yolculuk ekleme işlemi başarılı bir şekilde gerçekleşti' && $ekle_success_container.val() != ''){
    if ($modal_message.html().search($ekle_success_container.val()) == -1) {
      $modal_message.html($modal_message.html() + $ekle_success_container.val());
    }
  }
  //Modal'ı gizleyen veya gösteren bölüm.(Titreme efekti ile.)
  if ($modal_message.html() != '') {
    $modal.css("display", "block");
    $modal.removeClass("vibrate");
    setTimeout('$modal.addClass("vibrate");', 100);
  } else {
    $modal.css("display", "none");
  }
});

$kayit_btn.click(function () {
  $valid_mesafe = true;
  //Girdilerin dolu olduğunu kontrol eden bölüm.
  $modal_message.html('');
  for (var $i = 0; $i < $form_items.length; $i++) {
    if ($form_items[$i].val() == '' && $modal_message.html().search($err_contents[$i]) == -1) {
      $form_items[$i].siblings(".input-req").removeClass("input-req-success");
      $form_items[$i].siblings(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_contents[$i]);
    } else if ($form_items[$i].val() != '' && $modal_message.html().search($err_contents[$i]) != -1) {
      $form_items[$i].siblings(".input-req").addClass("input-req-success");
      $form_items[$i].siblings(".input-req").removeClass("input-req-fail");
      $modal_message.html($modal_message.html().replace($err_contents[$i], ""));
    }
  }

  //E-posta adresinin geçerli olup olmadığını kontrol eden bölüm.
  if ($mesafe.val() != '') {
    console.log("not empty");
    if ($mesafe.val()>500) {
      $valid_mesafe = false;
      $mesafe.siblings(".input-req").removeClass("input-req-success");
      $mesafe.siblings(".input-req").addClass("input-req-fail");
      $modal_message.html($modal_message.html() + $err_valid_mesafe);
    }
  }

  //Modal'ı gizleyen veya gösteren bölüm.(Titreme efekti ile.)
  if ($modal_message.html() != '') {
    $modal.css("display", "block");
    $modal.removeClass("vibrate");
    setTimeout('$modal.addClass("vibrate");', 100);
  }
  else {
    $modal.css("display", "none");
  }

});

//Modal'ı kapatan buton için click handler.
$close_btn.click(function () {
  $modal.css("display", "none");
});

$kayit_btn.click(function () {
  $("#form").submit();
});

$("input").focus(function () {
  $(this).siblings(".input-req").removeClass("input-req-fail");
  $(this).siblings(".input-req").addClass("input-req-success");
});

//Girdilerden biri boşsa submit işlemini iptal eden fonksyon.
function kayit_valid() {
  console.log($err_valid_mesafe);
  if ($tarih.val() == '' || $varis.val() == '' || $kalkis.val() == '' || $mesafe.val() == '' || !$valid_mesafe) {
    return false;
  }
}