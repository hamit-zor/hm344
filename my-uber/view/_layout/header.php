<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $model['title']; ?></title>
    <link rel="icon" href="./resources/images/icon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./view/_layout/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="./view/_layout/css/global.css">  
    <?php
    foreach ($css as $cssfile) {
      echo '<link rel="stylesheet" href="./view/_layout/css/'.$cssfile.'">';
    }
    ?>
  </head>

  <body>