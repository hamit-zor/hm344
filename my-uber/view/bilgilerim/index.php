<input id="duzenle_success_container" type="hidden" value="<?php echo $model['duzenle_success'] ?>">
<input id="parolami_degistir_success_container" type="hidden" value="<?php echo $model['parolami_degistir_success'] ?>">
<div class="modal">
  <div class="modal-content">
    <div class="btn-modal-close">
      <i class="fa fa-times-circle"></i>
    </div>
    <div class="modal-message">
    </div>
  </div>
</div>
<div class="wrapper header">
  <div class="h1">
    <a href="./?url=anasayfa">MyUber</a>
  </div>
  <div class="display-flex flex-row justify-content-between bar">
    <div>
      <a href="./?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
      <a href="./?url=yolculuklarim" class="button">Yolculuklarım</a>
    </div>
    <div>
      <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
    </div>
  </div>
  <div class="panel">
    <div class="panel-title">Kişisel Bilgilerim</div>
    <div class="panel-content">
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Ad</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="name" type="text" readonly value="<?php echo $model['user_name']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>Soyad</div>
          </div>
        </div>
        <div class="content-data">
          <div class="form-l-i">
            <input id="sirname" type="text" readonly value="<?php echo $model['user_sirname']; ?>">
          </div>
        </div>
      </div>
      <div class="display-flex flex-row">
        <div class="content-label">
          <div class="form-l-i">
            <div>E-posta adresi</div>
          </div>
        </div>
        <div class="content-data-email">
          <div class="form-l-i">
            <input id="email" type="text" readonly value="<?php echo $model['user_email']; ?>">
          </div>
        </div>
      </div>
    </div>

    <div class="form-b">
      <div>
        <a href="./?url=bilgilerim/duzenle" id="btn-login" class="button">Düzenle</a>
        <a href="./?url=bilgilerim/parolami-degistir" id="btn-login" class="button">Parolamı Değiştir</a>
      </div>
    </div>
  </div>
</div>