<input id="parolami_degistir_success_container" type="hidden" value="<?php echo $model['parolami_degistir_success'] ?>">
<div class="modal">
  <div class="modal-content">
    <div class="btn-modal-close">
      <i class="fa fa-times-circle"></i>
    </div>
    <div class="modal-message">
    </div>
  </div>
</div>
<form id="form" method="post" action="./?url=bilgilerim/parolami-degistir-denemesi" onsubmit="return tamamla_valid()">
  <div class="wrapper header">
    <div class="h1">
      <a href="./?url=anasayfa">MyUber</a>
    </div>
    <div class="display-flex flex-row justify-content-between bar">
      <div>
        <a href="./?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
        <a href="./?url=yolculuklarim" class="button">Yolculuklarım</a>
      </div>
      <div>
        <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
      </div>
    </div>
    <div class="panel">
      <div class="panel-title">Parolamı Değiştir</div>
      <div class="panel-content">
        <div class="display-flex flex-row">
          <div class="display-flex flex-column content-label">
            <div class="form-l-i">
              <div>Eski parola</div>
            </div>
          </div>
          <div class="display-flex flex-column content-data">
            <div class="form-l-i">
              <input name="old-password" id="password-old" type="password" value="">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
        <div class="display-flex flex-row">
          <div class="display-flex flex-column content-label">
            <div class="form-l-i">
              <div>Yeni parola</div>
            </div>
          </div>
          <div class="display-flex flex-column content-data">
            <div class="form-l-i">
              <input name="new-password" id="password" type="password" value="">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
        <div class="display-flex flex-row">
          <div class="display-flex flex-column content-label">
            <div class="form-l-i">
              <div>Yeni parolayı tekrar girin</div>
            </div>
          </div>
          <div class="display-flex flex-column content-data">
            <div class="form-l-i">
              <input id="password-again" type="password" value="">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="form-b">
        <div>
          <a id="btn-parola-degistir" class="button">Parolamı Değiştir</a>
        </div>
      </div>
    </div>
  </div>
</form>