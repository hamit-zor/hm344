<input id="duzenle_success_container" type="hidden" value="<?php echo $model['duzenle_success'] ?>">
<form id="form" method="POST" action="?url=duzenleme-kaydetme-denemesi" onsubmit="return tamamla_valid()">
  <div class="modal">
    <div class="modal-content">
      <div class="btn-modal-close">
        <i class="fa fa-times-circle"></i>
      </div>
      <div class="modal-message">
      </div>
    </div>
  </div>
  <div class="wrapper header">
    <div class="h1">
      <a href="./?url=anasayfa">MyUber</a>
    </div>
    <div class="display-flex flex-row justify-content-between bar">
      <div>
        <a href="./?url=bilgilerim" class="button">Kişisel Bilgilerim</a>
        <a href="./?url=yolculuklarim" class="button">Yolculuklarım</a>
      </div>
      <div>
        <a href="./?url=oturum-kapatma-denemesi" class="button">Çıkış Yap</a>
      </div>
    </div>
    <div class="panel">
      <div class="panel-title">Kişisel Bilgilerimi Düzenle</div>
      <div class="panel-content">
        <div class="display-flex flex-row">
          <div class="content-label">
            <div class="form-l-i">
              <div>Ad</div>
            </div>
          </div>
          <div class="content-data">
            <div class="form-l-i">
              <input name="name" id="name" type="text" value="<?php echo $model['name_attempt']; ?>" placeholder="<?php echo $model['user_name']; ?>">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
        <div class="display-flex flex-row">
          <div class="content-label">
            <div class="form-l-i">
              <div>Soyad</div>
            </div>
          </div>
          <div class="content-data">
            <div class="form-l-i">
              <input name="sirname" id="sirname" type="text" value="<?php echo $model['sirname_attempt']; ?>" placeholder="<?php echo $model['user_sirname']; ?>">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
        <div class="display-flex flex-row">
          <div class="content-label">
            <div class="form-l-i">
              <div>E-posta adresi</div>
            </div>
          </div>
          <div class="content-data-email">
            <div class="form-l-i">
              <input name="email" id="email" type="text" value="<?php echo $model['email_attempt']; ?>" placeholder="<?php echo $model['user_email']; ?>">
              <i class="fa fa-times input-req input-req-success"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="form-b">
        <div>
          <a id="btn-tamamla" class="button">Düzenlemeyi Tamamla</a>
          <a href="?url=bilgilerim" id="btn-iptal" class="button">İptal Et</a>
        </div>
      </div>
    </div>
  </div>
</form>