<?php

include './controller/CONTROLLER.php';

if (!empty($_GET['url'])) {
  $url = $_GET['url'];
  switch ($url) {
    case 'anasayfa':
      controller_router('anasayfa', 'anasayfa', 'index');
      break;
    case 'giris':
      controller_router('anasayfa', 'anasayfa', 'giris');
      break;
    case 'kayit':
      controller_router('anasayfa', 'anasayfa', 'kayit');
      break;
    case 'oturum-acma-denemesi':
      controller_router('anasayfa', 'anasayfa', 'oturum-acma-denemesi');
      break;
    case 'anasayfa-giris-yapildi':
      controller_router('anasayfa', 'anasayfa', 'anasayfa-giris-yapildi');
      break;
    case 'oturum-kapatma-denemesi':
      controller_router('anasayfa', 'anasayfa', 'oturum-kapatma-denemesi');
      break;
    case 'bilgilerim':
      controller_router('bilgilerim', 'bilgilerim', 'index');
      break;
    case 'bilgilerim/duzenle':
      controller_router('bilgilerim', 'bilgilerim', 'duzenle');
      break;
    case 'duzenleme-kaydetme-denemesi':
      controller_router('bilgilerim', 'bilgilerim', 'duzenleme-kaydetme-denemesi');
      break;
    case 'bilgilerim/parolami-degistir':
      controller_router('bilgilerim', 'bilgilerim', 'parolami-degistir');
      break;
    case 'bilgilerim/parolami-degistir-denemesi':
      controller_router('bilgilerim', 'bilgilerim', 'parolami-degistir-denemesi');
      break;
    case 'kayit-denemesi':
      controller_router('anasayfa', 'anasayfa', 'kayit-denemesi');
      break;
    case 'yolculuklarim':
      controller_router('yolculuk', 'yolculuk', 'index');
      break;
    case 'yolculuklarim/ekle':
      controller_router('yolculuk', 'yolculuk', 'ekle');
      break;
    case 'yolculuklarim/detay':
      controller_router('yolculuk', 'yolculuk', 'detay',array('id'=>$_GET['id']));
      break;
    case 'yolculuklarim/sofor-bilgileri':
      controller_router('yolculuk', 'yolculuk', 'sofor-bilgileri',array('id'=>$_GET['id']));
      break;
    case 'yolculuklarim/filtrelenmis':
      controller_router('yolculuk', 'yolculuk', 'filtrelenmis');
      break;
    case 'ekle-denemesi':
      controller_router('yolculuk', 'yolculuk', 'ekle-denemesi');
      break;
    default:
      controller_router('hatalar', NULL, 'index', array('hata_mesaji' => '\'url\' değişkeni hatalı girildi.<br>Eğer bu sayfaya yanlışlıkla geldiyseniz lütfen \'MyUber\' ikonuna tıklayınız.', 'hata_kodu' => '404', 'hata_kodu_aciklama' => 'Not Found'));
      break;
  }
} else {
  controller_router('anasayfa', 'anasayfa');
}
?>