<?php

function index($parameters) {
  session_start();
  if (isset($_SESSION['email'])) {
    header('location:?url=anasayfa-giris-yapildi');
  }
  $model['title'] = 'Anasayfa';

  sayfa('anasayfa/index.php', $model);
}

function giris($parameters) {
  session_start();

  if (isset($_SESSION['email'])) {
    header('location:?url=anasayfa-giris-yapildi');
  }

  $model['title'] = 'Giriş';

  if (isset($_SESSION['email_attempt'])) {
    $model['email_attempt'] = $_SESSION['email_attempt'];
  } else {
    $model['email_attempt'] = '';
  }
  if (isset($_SESSION['login_success'])) {
    if ($_SESSION['login_success'] == 'false') {
      $_SESSION['login_success'] = 'true';
      $model['login_success'] = '_invalid_email_password_pair';
    } else {
      $model['login_success'] = 'valid_email_password_pair';
    }
  } else {
    $model['login_success'] = 'valid_email_password_pair';
  }
  if (isset($_SESSION['kayit_success'])) {
    $model['kayit_success'] = $_SESSION['kayit_success'];
    $_SESSION['kayit_success'] = '';
  } else {
    $model['kayit_success'] = '';
  }
  sayfa('anasayfa/giris.php', $model, array('giris.css'), array('giris.js'));
}

function kayit($parameters) {
  session_start();

  $model['title'] = 'Kayit Ol';
  if (isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
    exit();
  }
  if (!isset($_SESSION['name_attempt'])) {
    $_SESSION['name_attempt'] = '';
  }
  if (!isset($_SESSION['sirname_attempt'])) {
    $_SESSION['sirname_attempt'] = '';
  }
  if (!isset($_SESSION['email_attempt'])) {
    $_SESSION['email_attempt'] = '';
  }
  if (!isset($_SESSION['password_attempt'])) {
    $_SESSION['password_attempt'] = '';
  }


  $model['email_attempt'] = $_SESSION['email_attempt'];
  $model['name_attempt'] = $_SESSION['name_attempt'];
  $model['sirname_attempt'] = $_SESSION['sirname_attempt'];
  $model['password_attempt'] = $_SESSION['password_attempt'];

  if (isset($_SESSION['kayit_success'])) {
    $model['kayit_success'] = $_SESSION['kayit_success'];
    $_SESSION['kayit_success'] = '';
  } else {
    $model['kayit_success'] = '';
  }

  sayfa('anasayfa/kayit.php', $model, array('kayit.css'), array('kayit.js'));
}

function kayit_denemesi($parameters) {
  session_start();
  $_SESSION['name_attempt'] = '';
  $_SESSION['sirname_attempt'] = '';
  $_SESSION['email_attempt'] = '';
  $_SESSION['password_attempt'] = '';
  $_SESSION['duzenle_success'] = '';

  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('location:./?url=kayit');
    exit();
  }

  $name = $_POST['name'];
  $sirname = $_POST['sirname'];
  $email = $_POST['email'];
  $password = $_POST['password'];

  if ($email == "" or $email == NULL or $name == "" or $name == NULL or $sirname == "" or $sirname == NULL or $password == "" or $password == NULL) {
    $_SESSION['kayit_success'] = 'Tüm bölümler doldurulmalıdır.';
    header('location:./?url=kayit');
    exit();
  }

  $_SESSION['name_attempt'] = $name;
  $_SESSION['sirname_attempt'] = $sirname;
  $_SESSION['email_attempt'] = $email;
  $_SESSION['password_attempt'] = $password;


  $answer = kullanici_kayit_et($name, $sirname, $email, $password);

  $_SESSION['kayit_success'] = $answer;

  if ($answer == 'Kayıt olma işlemi başarılı bir şekilde gerçekleşti.') {
    header('location:./?url=giris');
  } else {
    header('location:./?url=kayit');
  }
  exit();
}

function oturum_acma_denemesi($parameters) {
  session_start();
  $_SESSION['email_attempt'] = $_POST['email'];
  $_SESSION['login_success'] = 'true';
  $email = $_POST['email'];
  $passwordAttempt = $_POST['password'];

  //Kullanıcı formdan mı geliyor kontrol et.
  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('location:./?url=giris');
    exit();
  }
  //Tüm bilgiler tamam mı kontrol et.
  $email = $_POST['email'];
  $passwordAttempt = $_POST["password"];

  if ($email == "" or $email == NULL or $passwordAttempt == "" or $passwordAttempt == NULL) {
    $_SESSION['login_success'] = 'false';
    header('location:./?url=giris');
    exit();
  }

  $row = kullanici_bilgisini_al($email);


  if ($row == NULL) {
    $_SESSION['login_success'] = 'false';
    header('location:./?url=giris');
    exit();
  }
  $password = $row['kullanici_parola'];
  $id = $row['kullanici_id'];

  if ($password != hash('sha256', $passwordAttempt)) {
    $_SESSION['login_success'] = 'false';
    header('location:./?url=giris');
    exit();
  } else {
    //All checks complete. Start session
    $_SESSION['email'] = $email;
    $_SESSION['kullanici_id'] = $id;
    $_SESSION['login_success'] = 'true';
    header('location: ./?url=anasayfa');
    exit();
  }
}

function anasayfa_giris_yapildi($parameters) {
  session_start();
  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }

  $model['title'] = 'Anasayfa';
  $user_data = kullanici_bilgisini_al($_SESSION['email']);
  $model['user_id'] = $user_data['kullanici_id'];
  $model['user_name'] = $user_data['kullanici_ad'];
  $model['user_sirname'] = $user_data['kullanici_soyad'];
  $model['user_email'] = $user_data['kullanici_eposta'];

  sayfa('anasayfa/index_giris_yapildi.php', $model, array('anasayfa-giris-yapildi.css'), array());
}

function oturum_kapatma_denemesi($parameters) {
  session_start();

  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }

  session_unset();
  session_destroy();
  header('location:?url=giris');
}

?>
