<?php

function index($parameters) {
  session_start();

  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }

  $model['title'] = 'Yolculuklarım';
  $user_data = get_user_data($_SESSION['email']);
  $model['user_id'] = $user_data['kullanici_id'];

  $yolculuklar = get_yolculuklar($model['user_id']);

  if (isset($_SESSION['ekle_success'])) {
    $model['ekle_success'] = $_SESSION['ekle_success'];
    $_SESSION['ekle_success'] = '';
  } else {
    $model['ekle_success'] = '';
  }

  if ($yolculuklar == NULL) {
    $model['yolculuklar'] = array();
    $model['yolculuk_aciklama'] = 'Hiç yolculuğunuz bulunmamaktadır.';
    $model['yolculuk_aciklama_css'] = 'yolculuk-aciklama-failed';
  } else {
    $model['yolculuklar'] = $yolculuklar;
    $model['yolculuk_aciklama'] = 'Toplam ' . count($yolculuklar) . ' yolculuğunuz bulunmaktadır.';
    $model['yolculuk_aciklama_css'] = 'yolculuk-aciklama-success';
  }

  sayfa('yolculuk/index.php', $model, array('yolculuklarim.css'), array('yolculuklarim.js'));
}

function ekle($parameters) {
  session_start();

  $model['title'] = 'Yolculuk Ekle';
  if (!isset($_SESSION['email'])) {
    header('location:?url=giris');
    exit();
  }

  if (!isset($_SESSION['tarih_attempt'])) {
    $_SESSION['tarih_attempt'] = '';
  }
  if (!isset($_SESSION['kalkis_attempt'])) {
    $_SESSION['kalkis_attempt'] = '';
  }
  if (!isset($_SESSION['varis_attempt'])) {
    $_SESSION['varis_attempt'] = '';
  }
  if (!isset($_SESSION['mesafe_attempt'])) {
    $_SESSION['mesafe_attempt'] = '';
  }
  if (!isset($_SESSION['il_attempt'])) {
    $_SESSION['il_attempt'] = '';
  }
  if (!isset($_SESSION['sofor_attempt'])) {
    $_SESSION['sofor_attempt'] = '';
  }

  $model['tarih_attempt'] = $_SESSION['tarih_attempt'];
  $model['kalkis_attempt'] = $_SESSION['kalkis_attempt'];
  $model['varis_attempt'] = $_SESSION['varis_attempt'];
  $model['mesafe_attempt'] = $_SESSION['mesafe_attempt'];
  $model['il_attempt'] = $_SESSION['il_attempt'];
  $model['sofor_attempt'] = $_SESSION['sofor_attempt'];

  $soforler = get_soforler();
  $iller = get_iller();

  $model['soforler'] = $soforler;
  $model['iller'] = $iller;

  if (isset($_SESSION['ekle_success'])) {
    $model['ekle_success'] = $_SESSION['ekle_success'];
    $_SESSION['ekle_success'] = '';
  } else {
    $model['ekle_success'] = '';
  }

  sayfa('yolculuk/ekle.php', $model, array('ekle.css'), array('ekle.js'));
}

function ekle_denemesi($parameters) {
  session_start();
  $_SESSION['tarih_attempt'] = '';
  $_SESSION['kalkis_attempt'] = '';
  $_SESSION['varis_attempt'] = '';
  $_SESSION['mesafe_attempt'] = '';
  $_SESSION['il_attempt'] = '';
  $_SESSION['sofor_attempt'] = '';

  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('location:./?url=kayit');
    exit();
  }

  $tarih = $_POST['tarih'];
  $kalkis = $_POST['kalkis'];
  $varis = $_POST['varis'];
  $mesafe = $_POST['mesafe'];
  $il = $_POST['il'];
  $sofor = $_POST['sofor'];

  if ($tarih == "" or $tarih == NULL or $kalkis == "" or $kalkis == NULL or $varis == "" or $varis == NULL or $mesafe == "" or $mesafe == NULL or $il == "" or $il == NULL or $sofor == "" or $sofor == NULL) {
    echo "here";
    $_SESSION['ekle_success'] = 'Tüm bölümler doldurulmalıdır.';
    header('location:./?url=yolculuklarim/ekle');
    exit();
  }

  $_SESSION['tarih_attempt'] = $tarih;
  $_SESSION['kalkis_attempt'] = $kalkis;
  $_SESSION['varis_attempt'] = $varis;
  $_SESSION['mesafe_attempt'] = $mesafe;
  $_SESSION['il_attempt'] = $il;
  $_SESSION['sofor_attempt'] = $sofor;
  $id = $_SESSION['kullanici_id'];

  $answer = yolculuk_ekle($tarih, $kalkis, $varis, $mesafe, $il, $sofor, $id);

  $_SESSION['ekle_success'] = $answer;

  if ($answer == 'Yolculuk ekleme işlemi başarılı bir şekilde gerçekleşti.') {
    header('location:./?url=yolculuklarim');
  } else {
    header('location:./?url=yolculuklarim/ekle');
  }
  exit();
}

function sofor_bilgileri($parameters) {
  session_start();

  $model['title'] = 'Şoför Bilgileri';
  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }
  if (!isset($_GET['id'])) {
    header('location:./');
  }
  $sofor_data = get_sofor($parameters['id']);

  $model['sofor_ad'] = $sofor_data['sofor_ad'];
  $model['sofor_soyad'] = $sofor_data['sofor_soyad'];
  $model['sofor_eposta'] = $sofor_data['sofor_eposta'];

  sayfa('yolculuk/sofor-bilgileri.php', $model, array('bilgilerim.css'), array());
}

function detay($parameters) {
  session_start();

  $model['title'] = 'Yolculuk Detay';
  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }
  if (!isset($_GET['id'])) {
    header('location:./');
  }
  $sofor_data = get_yolculuk_detay($parameters['id']);

  $model['yolculuk_id'] = $sofor_data['yolculuk_id'];
  $model['yolculuk_kalkis_yeri'] = $sofor_data['yolculuk_kalkis_yeri'];
  $model['yolculuk_varis_yeri'] = $sofor_data['yolculuk_varis_yeri'];
  $model['yolculuk_tarih'] = $sofor_data['yolculuk_tarih'];
  $model['yolculuk_mesafe'] = $sofor_data['yolculuk_mesafe'];
  $model['yolculuk_fiyat'] = $sofor_data['yolculuk_fiyat'];
  $model['yolculuk_tarih'] = $sofor_data['yolculuk_tarih'];
  $model['il_isim'] = $sofor_data['il_isim'];
  $model['sofor_ad'] = $sofor_data['sofor_ad'];
  $model['sofor_soyad'] = $sofor_data['sofor_soyad'];

  sayfa('yolculuk/detay.php', $model, array('yolculuk-detay.css'), array());
}

function filtrelenmis($parameters) {
  session_start();

  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }
  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('location:?url=anasayfa');
    exit();
  }

  $model['title'] = 'Filtrelenmis';
  
  $user_data = get_user_data($_SESSION['email']);
  $model['user_id'] = $user_data['kullanici_id'];
  $model['baslangic'] = $_POST['baslangic'];
  $model['bitis'] = $_POST['bitis'];
  
  $yolculuklar = get_filtrelenmis_yolculuklar($model['user_id'],$model['baslangic'],$model['bitis']);

  if ($yolculuklar == NULL) {
    $model['yolculuklar'] = array();
    $model['yolculuk_aciklama'] = 'Bu tarihler arasında hiç yolculuğunuz bulunmamaktadır.';
    $model['yolculuk_aciklama_css'] = 'yolculuk-aciklama-failed';
  } else {
    $model['yolculuklar'] = $yolculuklar;
    $model['yolculuk_aciklama'] = 'Bu tarihler arasında toplam ' . count($yolculuklar) . ' yolculuğunuz bulunmaktadır.';
    $model['yolculuk_aciklama_css'] = 'yolculuk-aciklama-success';
  }
  sayfa('yolculuk/filtrelenmis.php', $model, array('filtrelenmis.css'), array());
}

?>