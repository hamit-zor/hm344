<?php

set_include_path('');
include './resources/inc/user_defined_functions.php';
include './view/_layout/VIEW.php';
include './model/MODEL.php';

function controller_router($controller, $model_controller = NULL, $view = 'index', $parameters = array()) {
  include './controller/' . $controller . '.php';
  model_router($model_controller);
  $view = convert_url_to_controller_name($view);
  $view($parameters);
}

?>