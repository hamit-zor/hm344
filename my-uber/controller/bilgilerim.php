<?php

function index($parameters) {
  session_start();

  $model['title'] = 'Bilgilerim';
  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }
  
  if (isset($_SESSION['parolami_degistir_success'])) {
    $model['parolami_degistir_success'] = $_SESSION['parolami_degistir_success'];
    $_SESSION['parolami_degistir_success'] = '';
  } else {
    $model['parolami_degistir_success'] = '';
  }

  if (isset($_SESSION['duzenle_success'])) {
    $model['duzenle_success'] = $_SESSION['duzenle_success'];
    $_SESSION['duzenle_success'] = '';
  } else {
    $model['duzenle_success'] = '';
  }

  $user_data = get_user_data($_SESSION['email']);
  $model['user_id'] = $user_data['kullanici_id'];
  $model['user_name'] = $user_data['kullanici_ad'];
  $model['user_sirname'] = $user_data['kullanici_soyad'];
  $model['user_email'] = $user_data['kullanici_eposta'];

  sayfa('bilgilerim/index.php', $model, array('bilgilerim.css'), array('bilgilerim.js'));
}

function parolami_degistir($parameters) {
  session_start();

  $model['title'] = 'Parola Değiştir';
  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }

  if (isset($_SESSION['parolami_degistir_success'])) {
    $model['parolami_degistir_success'] = $_SESSION['parolami_degistir_success'];
    $_SESSION['parolami_degistir_success'] = '';
  } else {
    $model['parolami_degistir_success'] = '';
  }

  sayfa('bilgilerim/parolami-degistir.php', $model, array('parolami-degistir.css'), array('parolami-degistir.js'));
}

function parolami_degistir_denemesi($param) {
  session_start();

  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('location:./?url=bilgilerim');
    exit();
  }

  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }

  $old_password_attempt = $_POST['old-password'];
  $new_password = $_POST['new-password'];

  if ($old_password_attempt == "" or $old_password_attempt == NULL or $new_password == "" or $new_password == NULL) {
    $_SESSION['parolami_degistir_success'] = 'Tüm bölümler doldurulmalıdır.';
    header('location:./?url=bilgilerim/parolami-degistir');
    exit();
  }

  $row = kullanici_bilgisini_al($_SESSION['email']);


  $password = $row['kullanici_parola'];

  $id = $row['kullanici_id'];

  if ($password != hash('sha256', $old_password_attempt)) {
    $_SESSION['parolami_degistir_success'] = 'Parolanızı güncellerken hata oluştu : Parolanız doğrulanamadı.';
    header('location:./?url=bilgilerim/parolami-degistir');
    exit();
  } else {
    $answer = set_password($old_password, $new_password, $_SESSION['kullanici_id']);

    $_SESSION['parolami_degistir_success'] = $answer;

    if ($answer == 'Parolanız başarılı şekilde güncellendi.') {
      header('location:./?url=bilgilerim');
    } else {
      header('location:./?url=bilgilerim/parolami-degistir');
    }
    exit();
  }
}

function duzenle($parameters) {
  session_start();

  $model['title'] = 'Düzenle';
  if (!isset($_SESSION['email'])) {
    header('location:?url=anasayfa');
  }
  if (!isset($_SESSION['name_attempt'])) {
    $_SESSION['name_attempt'] = '';
  }
  if (!isset($_SESSION['sirname_attempt'])) {
    $_SESSION['sirname_attempt'] = '';
  }
  if (!isset($_SESSION['email_attempt'])) {
    $_SESSION['email_attempt'] = '';
  }

  $model['email_attempt'] = $_SESSION['email_attempt'];
  $model['name_attempt'] = $_SESSION['name_attempt'];
  $model['sirname_attempt'] = $_SESSION['sirname_attempt'];


  if (isset($_SESSION['duzenle_success'])) {
    $model['duzenle_success'] = $_SESSION['duzenle_success'];
    $_SESSION['duzenle_success'] = '';
  } else {
    $model['duzenle_success'] = '';
  }

  $user_data = get_user_data($_SESSION['email']);

  $model['user_id'] = $user_data['kullanici_id'];
  $model['user_name'] = $user_data['kullanici_ad'];
  $model['user_sirname'] = $user_data['kullanici_soyad'];
  $model['user_email'] = $user_data['kullanici_eposta'];

  sayfa('bilgilerim/duzenle.php', $model, array('duzenle.css'), array('duzenle.js'));
}

function duzenleme_kaydetme_denemesi($parameters) {
  session_start();

  if (!isset($_SESSION['name_attempt'])) {
    $_SESSION['name_attempt'] = '';
  }
  if (!isset($_SESSION['sirname_attempt'])) {
    $_SESSION['sirname_attempt'] = '';
  }
  if (!isset($_SESSION['email_attempt'])) {
    $_SESSION['email_attempt'] = '';
  }
  if (!isset($_SESSION['duzenle_success'])) {
    $_SESSION['duzenle_success'] = '';
  }

  if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    header('location:./?url=bilgilerim');
    exit();
  }
  $name = $_POST['name'];
  $sirname = $_POST['sirname'];
  $email = $_POST['email'];

  if ($email == "" or $email == NULL or $name == "" or $name == NULL or $sirname == "" or $sirname == NULL) {
    $_SESSION['duzenle_success'] = 'Tüm bölümler doldurulmalıdır.';
    header('location:./?url=bilgilerim/duzenle');
    exit();
  }

  $_SESSION['name_attempt'] = $name;
  $_SESSION['sirname_attempt'] = $sirname;
  $_SESSION['email_attempt'] = $email;


  $answer = set_user_data($name, $sirname, $email, $_SESSION['kullanici_id']);
  $_SESSION['duzenle_success'] = $answer;

  if ($answer == 'Bilgileriniz başarılı şekilde güncellendi.') {
    $_SESSION['email'] = $email;
    header('location:./?url=bilgilerim');
  } else {
    header('location:./?url=bilgilerim/duzenle');
  }
  exit();
}

?>
